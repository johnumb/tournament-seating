<?php

class ParticipantsController extends Controller 
{
    
    function view() {
        $this->set('title','Tournament Seating');
        $query = "
            SELECT participants.*
            FROM participants
        ";
        $this->set('data',$this->User->getArray('query',[$query, '', []]));
    }
    function printBlank(){
        $this->set('title','Tournament Seating');
        
    }
    
    
    /**
     * 
     */
    function printSeating($devision = null){
        $this->set('title','Tournament Seating');
        $query = "
            SELECT participants.*, dojos.abbrv
            FROM prticipants
            INNER JOIN dojos
                ON participants.dojoId = dojos.id
                    OR participants.dojoAltId = dojos.id
            ORDER BY participants.division, randomKataa
        ";
        $this->set('dataKata',$this->Participant->getArray('query',[$query, '', []]));
        $query = "
            SELECT participants.*, dojos.abbrv
            FROM participants
            INNER JOIN dojos
                ON participants.dojoId = dojos.id
                    OR participants.dojoAltId = dojos.id
            ORDER BY participants.division, randomKumite
        ";
        $this->set('dataKumite',$this->Participant->getArray('query',[$query, '', []]));
        $query = "
            SELECT devisions.*
            FROM participants
            INNER JOIN devisions
                ON participants.division = devisions.division
            WHERE participants.kata != '-'
            GROUP BY participants.division
        ";
        $this->set('kata',$this->Participant->getArray('query',[$query, '', []]));
        $query = "
            SELECT devisions.*
            FROM participants
            INNER JOIN devisions
                ON participants.division = devisions.division
            WHERE participants.kumite != '-'
            GROUP BY participants.division
        ";
        $this->set('kumite',$this->Participant->getArray('query',[$query, '', []]));
    }
    
    function randomize($kata=false, $kumite=false){
        $this->set('ajax', true);
        if($kata){
            $query = "
                SELECT devisions.*
                FROM participants
                INNER JOIN devisions
                    ON participants.kata = devisions.kata
                WHERE participants.kata != '-'
                GROUP BY participants.division
            ";
            $kata = $this->Participant->getArray('query',[$query, '', []]);
            echo "<pre>";
            foreach($kata AS $value){
                $order = $this->getParticipants($value, 0, 0);
                print_r($order);
                foreach($order AS $key=>$value){
                    $query = "
                        UPDATE participants
                        SET randomKata = ?
                        WHERE id = ?
                    ";
                    $this->Participant->query($query, 'ii', array($key, $value));
                }
            }
            
        } else {
            $query = "
                SELECT devisions.*
                FROM participants
                INNER JOIN devisions
                    ON participants.kumite = devisions.kumite
                WHERE participants.kumite != '-'
                GROUP BY participants.division
            ";
            $kata = $this->Participant->getArray('query',[$query, '', []]);
            echo "<pre>";
            foreach($kata AS $value){
                $order = $this->getParticipantsS($value, 0, 0);
                print_r($order);
                foreach($order AS $key=>$value){
                    $query = "
                        UPDATE participants
                        SET randomKumite = ?
                        WHERE id = ?
                    ";
                    $this->Participant->query($query, 'ii', array($key, $value));
                }
            }
        }
    }
    
    
    function getParticipants($kata, $UsedIds, $lastDojo, $return = array()){
        $query = "
            SELECT participants.id, if(dojoAltId IS NULL, dojoId, dojoAltId) AS dojoId, kata, kumite
            FROM participants
            WHERE participants.division = '".$kata['division']."'
                AND id NOT IN ($UsedIds)
            ORDER BY kata, dojoId
        ";
        $results= $this->Participant->getArray('query',[$query, '', []]);
        $contestent_picked = false;
        foreach($results AS $key => $value){
            if($lastDojo !== $value['dojoId']){
                array_push($return, $value['id']);
                $UsedIds .= ','.$value['id'];
                $lastDojo = $value['dojoId'];
                $contestent_picked = true;
                break;
            }
        }
        if(!$contestent_picked){
            array_push($return, $results[0]['id']);
            $UsedIds .= ','.$value['id'];
            $lastDojo = $results[0]['dojoId'];
        }
        if(count($results) > 1){
            $return = $this->getParticipants($kata, $UsedIds, $lastDojo, $return);
        }
        return $return;
    }
    
    function getParticipantsS($kata, $UsedIds, $lastDojo, $return = array()){
        $query = "
            SELECT participants.id, if(dojoAltId IS NULL, dojoId, dojoAltId) AS dojoId, kata, kumite
            FROM participants
            WHERE participants.division = '".$kata['division']."'
                AND id NOT IN ($UsedIds)
            ORDER BY kumite, dojoId
        ";
        $results= $this->Participant->getArray('query',[$query, '', []]);
        $contestent_picked = false;
        foreach($results AS $key => $value){
            if($lastDojo !== $value['dojoId']){
                array_push($return, $value['id']);
                $UsedIds .= ','.$value['id'];
                $lastDojo = $value['dojoId'];
                $contestent_picked = true;
                break;
            }
        }
        if(!$contestent_picked){
            array_push($return, $results[0]['id']);
            $UsedIds .= ','.$value['id'];
            $lastDojo = $results[0]['dojoId'];
        }
        if(count($results) > 1){
            $return = $this->getParticipants($kata, $UsedIds, $lastDojo, $return);
        }
        return $return;
    }
}