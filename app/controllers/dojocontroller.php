<?php

class DojoController extends Controller 
{
    function put($dojo = ''){
        echo $dojo;
        $this->set('ajax', true);
        $query = "
            INSERT INTO dojos
            SET dojo = ?
                , abbrv = ''
        ";
        $this->Dojo->query($query, 's', [$dojo]);
        $query = "
            SELECT id
            FROM dojos
            WHERE dojo = ?
        ";
        $record = $this->Dojo->getArray('query',[$query, 's', [$dojo]]);
        return $record[0]['id'];
    }
    
    
}