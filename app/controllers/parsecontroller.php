<?php

class ParseController extends Controller 
{
    function form(){
        $this->set('title', 'Tournament Seating - Parser Form');
    }
    
    function parse(){
        $this->set('title', 'Tournament Seating - Parser');
    }
    /**
     *     
    [0] => Submission Date
    [1] => First Name
    [2] => Last Name
    [3] => Competitor Date of Birth
    [4] => Gender
    [5] => E-mail
    [6] => Phone Number
    [7] => Street Address
    [8] => Street Address Line 2
    [9] => City
    [10] => State / Province
    [11] => Postal / Zip Code
    [12] => Country
    [13] => Event Participation
    [14] => Rank in Kyu or Dan ie 5 Kyu or 1st Dan
    [15] => Years of Training
    [16] => Dojo Name
    [17] => Other Dojo Name
    [18] => Experience Level
    [19] => Emergency Contact Name
    [20] => Emergency Contact Phone Number
    [21] => Gloves, Shin Protection, Mouthguard, Groin (boys), and my own red and blue belts
    [22] => Name of Guardian if under 18 (First Name)
    [23] => Name of Guardian if under 18 (Last Name)
    [24] => Signature (Self or Guardian if under 18)
    [25] => Age
    [26] => 11/14/2015
    [27] => Age Range
    [28] => Division Name
    [29] => Kata Division
    [30] => Kumite Division
     */
    function put(){
        $dojo = new DojoController('Dojo', 'DojoController', '');
        $this->set('title', 'Tournament Seating - Parser');
        $csv = array_map('str_getcsv', file($_FILES['file']['tmp_name']));
        echo "<pre>";
        foreach( $csv AS $key => $line ){
            $particapent = [];
            if( (int)$key > 0 ){
                $particapent['dojoId'] = $dojo->put($line['16']);
                if($line[17] !== '') {
                    $particapent['dojoAltId'] = $dojo->put($line['17']);
                }
                $particapent['firstName'] = $line[1];
                $particapent['lastName'] = $line[2];
                $particapent['birthDate'] = $line[3];
                $particapent['gender'] = $line[4];
                $particapent['email'] = $line[5];
                $particapent['phone'] = $line[6];
                $particapent['address'] = $line[7];
                $particapent['addressExt'] = $line[8];
                $particapent['city'] = $line[9];
                $particapent['state'] = $line[10];
                $particapent['zip'] = $line[11];
                $particapent['country'] = $line[12];
                $particapent['participation'] = $line[13];
                $particapent['rank'] = $line[14];
                $particapent['yearsTraining'] = $line[15];
                $particapent['experiance'] = $line[18];
                $particapent['emergancyContact'] = $line[19];
                $particapent['emergancyNumber'] = $line[20];
                $particapent['TOS'] = ($line[21] != '' ? 1 : 0);
                $particapent['guardianFirstName'] = $line[22];
                $particapent['guardianLastName'] = $line[23];
                $particapent['signature'] = $line[24];
                $particapent['age'] = $line[25];
                $particapent['ageRange'] = $line[27];
                $particapent['division'] = $line[28];
                $particapent['kata'] = $line[29];
                $particapent['kumite'] = $line[30];
                $this->Parse->createInsert($particapent);
            } else {
                print_r( $line );
            }
        }
    }
    
    
}