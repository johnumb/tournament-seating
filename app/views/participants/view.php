
<h2>
    Participants
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#customModal">
        <i class="fa fa-plus-square"></i> Add Particapant
    </button>
</h2>
<table class="table table-striped table-hover income">
    <thead>
        <th>Income</th>
        <th>Frequency</th>
        <th>Amount</th>
        <th>Start Date</th>
        <th>End Date</th>
    </thead>
    <tbody>
    </tbody>
</table>

<div class="modal fade" id="customModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" id="formSave">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Budget Items</h4>
            </div>
            <div class="modal-body">
                
                <input type="hidden" name="id" id="id" value="">
                <div class="col-sm-12">
                    <label class="control-label col-sm-4" for="income">Income:</label>
                    <div class="col-sm-8 form-group-sm">
                        <input type="text" name="income" id="income" value="" placeholder="Income Name" class="form-control">
                    </div>
                </div>
                <div class="col-sm-12">
                    <label class="control-label col-sm-4" for="bill">Frequency:</label>
                    <div class="col-sm-8 form-group-sm">
                        <select name="frequencyId" id="frequencyId" class="form-control">
                            <?php
                            foreach($frequencyData AS $frequency){
                                echo '<option value="'.$frequency['id'].'">'.$frequency['frequency'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label class="control-label col-sm-4" for="bill">Amount:</label>
                    <div class="col-sm-8 form-group-sm">
                        <div class="input-group margin-ten">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-dollar"></i></span>
                            <input type="text" name="amount" id="amount" value="" placeholder="Amount Of Bill" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label class="control-label col-sm-4" for="startDate">Start Date :</label>
                    <div class="col-sm-8 form-group-sm">
                        <div class="input-group margin-ten">
                            <input type="date" name="startDate" id="startDate" value="" placeholder="Date you want this to start" class="form-control">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label class="control-label col-sm-4" for="endDate">End Date :</label>
                    <div class="col-sm-8 form-group-sm">
                        <div class="input-group margin-ten">
                            <input type="date" name="endDate" id="endDate" value="" placeholder="Date you want this to end" class="form-control">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="modal-footer">
                <button type="button" class="btn btn-default cancel" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save" data-dismiss="modal">Save changes</button>
                <button type="button" class="btn btn-warning remove" data-dismiss="modal">Remove</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    var table;
    $(document).ready(function(){
        $('.income').on( 'draw.dt', function () {
            $(document).off('click', '.edit').on('click', '.edit', function(){
                var data = $(this).parent().data();
                var jqXHR = $.ajax({
                    url : '/income/details/'+data.id+'/'+data.name+'/'
                    , type : 'POST'
                    , dataType : 'JSON'
                });
                $.when(jqXHR).done(function(data){
                    $.each(data, function(key, value){
                        if(typeof $('#'+key)[0] !== 'undefined'){
                            // Leaving this if it needed - var type = $('#'+key)[0].type;
                            $('#'+key).val(value);
                        }
                    });
                    $('#customModal').modal('toggle');
                });
            });
        });
        table = $('.income').dataTable();
        
        $('.save').click(function(event){
            event.preventDefault();
            var formData = $('#formSave').serialize();
            $.ajax({
                url : '/income/put'
                , type : 'post'
                , data : formData
            });
            load();
        });
        
        $('.remove').click(function(event){
            event.preventDefault();
            var data = $('#formSave').serialize();
            var jqXHR = $.ajax({
                url : '/income/remove'
                , type : 'POST'
                , data : data
            });
            load();
        });
        
        $('#customModal').on('hidden.bs.modal', function (e) {
            $('#formSave input').val('');
            $('#formSave select').val(0);
        });
        load();
    });
    
    function load(){
        table.fnDestroy(false);
        var jqXHR = $.ajax({
            url: '/income/get'
            , type : 'POST'
            , dataType : 'JSON'
        });
        
        $.when(jqXHR).done(function(data){
            console.log(data);
            $('.income tbody').html('');
            $.each(data, function(key, value){
                var html = ""+ 
                "<tr data-id=\""+value.id+"\" data-name=\""+value.income+"\">\n\
                    <td class=\"edit\">"+value.income+"</td>\n\
                    <td class=\"edit\">"+value.frequency+"</td>\n\
                    <td class=\"edit\">$"+value.amount+"</td>\n\
                    <td class=\"edit\">"+value.startDate+"</td>\n\
                    <td class=\"edit\">"+value.endDate+"</td>\n\
                </tr>\n\
                ";
                $('.income tbody').append(html);
            });
            table = $('.income').dataTable();
        });
    }
</script>