<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="/css/jquery-ui.structure.css" rel="stylesheet" type="text/css"/>
        <link href="/css/jquery-ui.theme.css" rel="stylesheet" type="text/css"/>
        <link href="/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,dt-1.10.9/datatables.min.css"/>
 
        <link href="/css/style.css" rel="stylesheet" type="text/css"/>
        <script src="/js/jquery-2.1.4.min.js" type="text/javascript"></script>
        
        <script src="/js/jquery-ui.js" type="text/javascript"></script>
        
        <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,dt-1.10.9/datatables.min.js"></script>
    </head>
    <body>