<style>
    .title{
        font-weight: bold;
        text-align: center;
        font-size: 16px;
    }
    .sheet{
        page-break-after:always;
    }
    .box-left-a{
        font-weight: bold;
        border-bottom: 1px solid #000;
    }
    .box-left-b{
        border-right: 1px solid #000;
        border-bottom: 1px solid #000;
        font-weight: bold;
    }
    .align-right{
        text-align: right;
        font-weight: bold;
    }
    .align-left{
        text-align: left;
        font-weight: bold;
    }
    .box-right-a{
        font-weight: bold;
        border-bottom: 1px solid #000;
        text-align: right;
    }
    .box-right-b{
        text-align: right;
        border-left: 1px solid #000;
        border-bottom: 1px solid #000;
        font-weight: bold;
    }
    .box-bottom{
        
    }
    .first-column-size{
        width: 110px;
        height: 30px;
        font-size: 8px;
    }
    .second-column-size{
        width: 110px;
        height: 60px;
        font-size: 8px;
    }
    .first-second-column-size{
        width: 110px;
        height: 45px;
        font-size: 8px;
    }
    .third-column-size{
        width: 110px;
        height: 120px;
        font-size: 8px;
    }
    .first-thrid-column-size{
        width: 110px;
        height: 75px;
        font-size: 8px;
    }
    .forth-column-size{
        width: 110px;
        height: 135px;
        font-size: 8px;
    }
    .column-1{
        margin-left: 11px;
    }
    .column-1, .column{
        float: left;
    }
    .name-first{
        line-height: 1.50;
    }
    .name-second-first{
        line-height: 5.25;
    }
    .name-third-lost, .name-second{
        line-height: 9;
    }
    .name-third-first{
        line-height: 12.75;
    }
    .name-third{
        line-height: 24;
    }
    .name-forth{
        line-height: 27.75;
    }
    .sheet{
        overflow: hidden;
        width: 925px;
    }
    .bar{
        border-bottom: 3px solid #000;
    }
    
    .lost-first-column-size
    , .lost-second-column-size
    , .lost-first-second-column-size
    , .lost-third-column-size
    , .lost-first-thrid-column-size{
        width: 147px;
    }
    .lost-first-column-size{
        height: 30px;
        font-size: 8px;
    }
    .lost-second-column-size{
        height: 30px;
        font-size: 8px;;
    }
    .lost-first-second-column-size{
        height: 45px;
        font-size: 8px;
    }
    .lost-third-column-size{
        height: 60px;
        font-size: 8px;
    }
    .lost-first-thrid-column-size{
        height: 75px;
        font-size: 8px;
    }
    .signature{
        margin-top: 5px;
        margin-left: 10px;
        font-weight: bold;
    }
    .startEnd{
        position: relative;
        top: -50px;
    }
    .devision{
        font-weight: bold;
        text-align: center;
    }
    .sig-spot{
        border-top: 1px solid #000000;
        width: 30%;
        text-align: center;
        float: left;
        margin-top: 30px;
        margin-left: 20px;
    }
</style>
<?php 
foreach($kata AS $value){
    $type = 'Kata';
$names = ['a1'=>''
        , 'a2'=>''
        , 'a3'=>''
        , 'a4'=>''
        , 'a5'=>''
        , 'a6'=>''
        , 'a7'=>''
        , 'a8'=>''
        , 'a9'=>''
        , 'a10'=>''
        , 'a11'=>''
        , 'a12'=>''
        , 'a13'=>''
        , 'a14'=>''
        , 'a15'=>''
        , 'b1'=>''
        , 'b2'=>''
        , 'b3'=>''
        , 'b4'=>''
        , 'b5'=>''
        , 'b6'=>''
        , 'b7'=>''
        , 'b8'=>''
        , 'b9'=>''
        , 'b10'=>''
        , 'b11'=>''
        , 'b12'=>''
        , 'b13'=>''
        , 'b14'=>''
        , 'b15'=>''];


    if($value['division'] != ''){
    $current = [];
    foreach( $dataKata AS $key => $value2 ){
        if(trim($value2['division']) == trim($value['division'])){
            array_push($current, $value2);
        }
    }
    
    $total = count($current);
    //$A = 15-round($total/2);
    $ACount = round($total/2);
    if($ACount > 4){
        $A = 1;
    } elseif($ACount > 2){
        $A = 9;
    } elseif($ACount > 1){
        $A = 13;
    } else {
        $A = 15;
    }
    //$B = 15-floor($total/2);
    $BCount = floor($total/2);
    $B = $A;
    $ADisplay = 0;
    $lastSide = '';
    foreach($current AS $key => $value2){
        if($ADisplay < $ACount){
            $names['a'.$A] = $value2['firstName'].' '.$value2['lastName'].' - '.$value2['abbrv'];
           $A++;
           $ADisplay++;
        } else {
            $names['b'.$B] = $value2['firstName'].' '.$value2['lastName'].' - '.$value2['abbrv'];
           $B++;
        }
    }
    extract($names);
?>
<div class="sheet">
    <div class="title">IMA Utah Karate Championship</div>
    <div class="devision">
        <?php echo $type.' Seating Chart';?>
    </div>
    <div class="devision">
        <?php echo $value['kata'].' '.$value['age'].' '.$value['gender'].' '.$value['level'];?>
    </div>
    <div class="startEnd">
        <span style="font-weight: bold;">Start Time: ___________________</span><br>
        <span style="font-weight: bold;">&nbsp;End Time: ___________________</span>
    </div>
    
    <div style='clear: both;'>
        <div class='column-1'>
            <div class="box-left-a first-column-size">
                &nbsp;
                <div class="name-first"><?php echo $a1;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a2;?></div>
            </div>
            <div class="box-left-a first-column-size">
                B
                <div class="name-first"><?php echo $a3;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a4;?></div>
            </div>
            <div class="box-left-a first-column-size">
                B
                <div class="name-first"><?php echo $a5;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a6;?></div>
            </div>
            <div class="box-left-a first-column-size">
                B
                <div class="name-first"><?php echo $a7;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a8;?></div>
            </div>
            <div class="align-left first-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a first-second-column-size">
                &nbsp;
                <div class="name-second-first"><?php echo $a9;?></div>
            </div>
            <div class='box-left-b second-column-size'>
                R
                <div class="name-second"><?php echo $a10;?></div>
            </div>

            <div class="box-left-a second-column-size">
                B
                <div class="name-second"><?php echo $a11;?></div>
            </div>

            <div class='box-left-b second-column-size'>
                R
                <div class="name-second"><?php echo $a12;?></div>
            </div>
            <div class="align-left second-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a first-thrid-column-size">
                &nbsp;
                <div class="name-third-first"><?php echo $a13;?></div>
            </div>
            <div class='box-left-b third-column-size'>
                R
                <div class="name-third"><?php echo $a14;?></div>
            </div>

            <div class="align-left third-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a forth-column-size">
                &nbsp;
                <div class="name-forth"><?php echo $a15;?></div>
            </div>
            <div class='align-left third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div style='width: 20px;'>&nbsp;</div>
        </div>
        <div class='column'>
            <div class="box-right-a forth-column-size">
                &nbsp;
                <div class="name-forth"><?php echo $b15;?></div>
            </div>
            <div class='align-right third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a first-thrid-column-size">
                &nbsp;
                <div class="name-third-first"><?php echo $b13;?></div>
            </div>
            <div class='box-right-b third-column-size'>
                R
                <div class="name-third"><?php echo $b14;?></div>    
            </div>

            <div class="align-right third-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a first-second-column-size">
                &nbsp;
                <div class="name-second-first"><?php echo $b9;?></div>
            </div>
            <div class='box-right-b second-column-size'>
                R
                <div class="name-second"><?php echo $b10;?></div>
            </div>

            <div class="box-right-a second-column-size">
                B
                <div class="name-second"><?php echo $b11;?></div>
            </div>

            <div class='box-right-b second-column-size'>
                R
                <div class="name-second"><?php echo $b12;?></div>
            </div>
            <div class="align-right second-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a first-column-size">
                &nbsp;
                <div class="name-first"><?php echo $b1;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b2;?></div>
            </div>
            <div class="box-right-a first-column-size">
                B
                <div class="name-first"><?php echo $b3;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b4;?></div>
            </div>
            <div class="box-right-a first-column-size">
                B
                <div class="name-first"><?php echo $b5;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b6;?></div>
            </div>
            <div class="box-right-a first-column-size">
                B
                <div class="name-first"><?php echo $b7;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b8;?></div>
            </div>
            <div class="align-right first-column-size">
                B
            </div>
        </div>
    </div>
    <div style='clear: both;' class='bar'></div>
    <div style='clear: both;'>
        <div class='column-1'>
            <div class="box-left-a lost-first-column-size">
                &nbsp;
                <div class="name-first"></div>
            </div>
            <div class='box-left-b lost-first-column-size'>
                R - 1st Round Loser To A
                <div class="name-first"></div>
            </div>
            <div class="align-left lost-first-column-size">
                B - 2nd Round Loser To A
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a lost-first-second-column-size">
                &nbsp;
                <div class="name-second-first"></div>
            </div>
            <div class='box-left-b lost-second-column-size'>
                R
                <div class="name-first"></div>
            </div>
            <div class="align-left lost-second-column-size">
                B - 3rd Round Loser To A
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a lost-third-column-size">
                &nbsp;
                <div class="name-third-lost"></div>
            </div>
            <div class='align-left lost-third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div style='width: 20px;'>&nbsp;</div>
        </div>
        <div class='column'>
            <div class="box-right-a lost-third-column-size">
                &nbsp;
                <div class="name-third-lost"></div>
            </div>
            <div class='align-right lost-third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a lost-first-second-column-size">
                &nbsp;
                <div class="name-second-first"></div>
            </div>
            <div class='box-right-b lost-second-column-size'>
                R
                <div class="name-first"></div>
            </div>
            <div class="align-right lost-second-column-size">
                3rd Round Loser to B - B
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a lost-first-column-size">
                &nbsp;
                <div class="name-first"></div>
            </div>
            <div class='box-right-b lost-first-column-size'>
                1st Round Loser To B - R
                <div class="name-first"></div>
            </div>
            <div class="align-right lost-first-column-size">
                2st Round Loser To B - B 
            </div>
        </div>
    </div>
    <div style='clear: both;' class='bar'></div>
    <div class="signature">SIGNATURES:</div>
    <div class="sig-spot">Judge #1</div>
    <div class="sig-spot">Judge #2</div>
    <div class="sig-spot">Judge #3</div>
    <div class="sig-spot">Judge #4</div>
    <div class="sig-spot">Arbitrator</div>
    <div class="sig-spot">Head Scorekeeper</div>
</div>
<?php
    }
}

foreach($kumite AS $value){
    $type = 'Kumite';
$names = ['a1'=>''
        , 'a2'=>''
        , 'a3'=>''
        , 'a4'=>''
        , 'a5'=>''
        , 'a6'=>''
        , 'a7'=>''
        , 'a8'=>''
        , 'a9'=>''
        , 'a10'=>''
        , 'a11'=>''
        , 'a12'=>''
        , 'a13'=>''
        , 'a14'=>''
        , 'a15'=>''
        , 'b1'=>''
        , 'b2'=>''
        , 'b3'=>''
        , 'b4'=>''
        , 'b5'=>''
        , 'b6'=>''
        , 'b7'=>''
        , 'b8'=>''
        , 'b9'=>''
        , 'b10'=>''
        , 'b11'=>''
        , 'b12'=>''
        , 'b13'=>''
        , 'b14'=>''
        , 'b15'=>''];


    if($value['kumite'] != ''){
    $current = [];
    foreach( $dataKumite AS $key => $value2 ){
        if(trim($value2['division']) == trim($value['division'])){
            array_push($current, $value2);
        }
    }
    
    $total = count($current);
    //$A = 15-round($total/2);
    $ACount = round($total/2);
    if($ACount > 4){
        $A = 1;
    } elseif($ACount > 2){
        $A = 9;
    } elseif($ACount > 1){
        $A = 13;
    } else {
        $A = 15;
    }
    //$B = 15-floor($total/2);
    $BCount = floor($total/2);
    $ADisplay = 0;
    $B = $A;
    $lastSide = '';
    foreach($current AS $key => $value2){
        if($ADisplay < $ACount){
            $names['a'.$A] = $value2['firstName'].' '.$value2['lastName'].' - '.$value2['abbrv'];
           $A++;
           $ADisplay++;
        } else {
            $names['b'.$B] = $value2['firstName'].' '.$value2['lastName'].' - '.$value2['abbrv'];
           $B++;
        }
    }
    extract($names);
?>
<div class="sheet">
    <div class="title">IMA Utah Karate Championship</div>
    <div class="devision">
        <?php echo $type.' Seating Chart';?>
    </div>
    <div class="devision">
        <?php echo $value['kumite'].' '.$value['age'].' '.$value['gender'].' '.$value['level'];?>
    </div>
    <div class="startEnd">
        <span style="font-weight: bold;">Start Time: ___________________</span><br>
        <span style="font-weight: bold;">&nbsp;End Time: ___________________</span>
    </div>
    
    <div style='clear: both;'>
        <div class='column-1'>
            <div class="box-left-a first-column-size">
                &nbsp;
                <div class="name-first"><?php echo $a1;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a2;?></div>
            </div>
            <div class="box-left-a first-column-size">
                B
                <div class="name-first"><?php echo $a3;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a4;?></div>
            </div>
            <div class="box-left-a first-column-size">
                B
                <div class="name-first"><?php echo $a5;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a6;?></div>
            </div>
            <div class="box-left-a first-column-size">
                B
                <div class="name-first"><?php echo $a7;?></div>
            </div>
            <div class='box-left-b first-column-size'>
                R
                <div class="name-first"><?php echo $a8;?></div>
            </div>
            <div class="align-left first-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a first-second-column-size">
                &nbsp;
                <div class="name-second-first"><?php echo $a9;?></div>
            </div>
            <div class='box-left-b second-column-size'>
                R
                <div class="name-second"><?php echo $a10;?></div>
            </div>

            <div class="box-left-a second-column-size">
                B
                <div class="name-second"><?php echo $a11;?></div>
            </div>

            <div class='box-left-b second-column-size'>
                R
                <div class="name-second"><?php echo $a12;?></div>
            </div>
            <div class="align-left second-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a first-thrid-column-size">
                &nbsp;
                <div class="name-third-first"><?php echo $a13;?></div>
            </div>
            <div class='box-left-b third-column-size'>
                R
                <div class="name-third"><?php echo $a14;?></div>
            </div>

            <div class="align-left third-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a forth-column-size">
                &nbsp;
                <div class="name-forth"><?php echo $a15;?></div>
            </div>
            <div class='align-left third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div style='width: 20px;'>&nbsp;</div>
        </div>
        <div class='column'>
            <div class="box-right-a forth-column-size">
                &nbsp;
                <div class="name-forth"><?php echo $b15;?></div>
            </div>
            <div class='align-right third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a first-thrid-column-size">
                &nbsp;
                <div class="name-third-first"><?php echo $b13;?></div>
            </div>
            <div class='box-right-b third-column-size'>
                R
                <div class="name-third"><?php echo $b14;?></div>    
            </div>

            <div class="align-right third-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a first-second-column-size">
                &nbsp;
                <div class="name-second-first"><?php echo $b9;?></div>
            </div>
            <div class='box-right-b second-column-size'>
                R
                <div class="name-second"><?php echo $b10;?></div>
            </div>

            <div class="box-right-a second-column-size">
                B
                <div class="name-second"><?php echo $b11;?></div>
            </div>

            <div class='box-right-b second-column-size'>
                R
                <div class="name-second"><?php echo $b12;?></div>
            </div>
            <div class="align-right second-column-size">
                B
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a first-column-size">
                &nbsp;
                <div class="name-first"><?php echo $b1;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b2;?></div>
            </div>
            <div class="box-right-a first-column-size">
                B
                <div class="name-first"><?php echo $b3;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b4;?></div>
            </div>
            <div class="box-right-a first-column-size">
                B
                <div class="name-first"><?php echo $b5;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b6;?></div>
            </div>
            <div class="box-right-a first-column-size">
                B
                <div class="name-first"><?php echo $b7;?></div>
            </div>
            <div class='box-right-b first-column-size'>
                R
                <div class="name-first"><?php echo $b8;?></div>
            </div>
            <div class="align-right first-column-size">
                B
            </div>
        </div>
    </div>
    <div style='clear: both;' class='bar'></div>
    <div style='clear: both;'>
        <div class='column-1'>
            <div class="box-left-a lost-first-column-size">
                &nbsp;
                <div class="name-first"></div>
            </div>
            <div class='box-left-b lost-first-column-size'>
                R - 1st Round Loser To A
                <div class="name-first"></div>
            </div>
            <div class="align-left lost-first-column-size">
                B - 2nd Round Loser To A
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a lost-first-second-column-size">
                &nbsp;
                <div class="name-second-first"></div>
            </div>
            <div class='box-left-b lost-second-column-size'>
                R
                <div class="name-first"></div>
            </div>
            <div class="align-left lost-second-column-size">
                B - 3rd Round Loser To A
            </div>
        </div>
        <div class='column'>
            <div class="box-left-a lost-third-column-size">
                &nbsp;
                <div class="name-third-lost"></div>
            </div>
            <div class='align-left lost-third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div style='width: 20px;'>&nbsp;</div>
        </div>
        <div class='column'>
            <div class="box-right-a lost-third-column-size">
                &nbsp;
                <div class="name-third-lost"></div>
            </div>
            <div class='align-right lost-third-column-size'>
                R
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a lost-first-second-column-size">
                &nbsp;
                <div class="name-second-first"></div>
            </div>
            <div class='box-right-b lost-second-column-size'>
                R
                <div class="name-first"></div>
            </div>
            <div class="align-right lost-second-column-size">
                3rd Round Loser to B - B
            </div>
        </div>
        <div class='column'>
            <div class="box-right-a lost-first-column-size">
                &nbsp;
                <div class="name-first"></div>
            </div>
            <div class='box-right-b lost-first-column-size'>
                1st Round Loser To B - R
                <div class="name-first"></div>
            </div>
            <div class="align-right lost-first-column-size">
                2st Round Loser To B - B 
            </div>
        </div>
    </div>
    <div style='clear: both;' class='bar'></div>
    <div class="signature">SIGNATURES:</div>
    <div class="sig-spot">Judge #1</div>
    <div class="sig-spot">Judge #2</div>
    <div class="sig-spot">Judge #3</div>
    <div class="sig-spot">Judge #4</div>
    <div class="sig-spot">Arbitrator</div>
    <div class="sig-spot">Head Scorekeeper</div>
</div>
<?php
    }
}