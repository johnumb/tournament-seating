<form method="post" action="/parse/put" enctype="multipart/form-data">
    <input type="hidden" name="users[id]" value="<?php echo $data[0]['id'];?>">
    
    <div class="col-sm-push-3 col-sm-pull-3 col-sm-6">
        <h2>Import</small></h2>
        <div class="col-sm-12">
            <label class="control-label col-sm-4" for="firstName">File (CSV Only):</label>
            <div class="col-sm-8 form-group-sm">
                <input type="file" name="file">
            </div>
        </div>
        <div class="col-sm-12">
            <label class="control-label col-sm-4"></label>
            <div class="col-sm-8 form-group-sm">
                <button class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Save</button>
            </div>
        </div>
    </div>
</form>
