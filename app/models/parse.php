<?php
class Parse extends Model
{
    function createInsert($data){
        $query = " 
            INSERT INTO participants 
            SET ";
        $params = [];
        $types = '';
        $count = 0;
        foreach($data AS $key => $value ){
            array_push($params, $value);
            if ( gettype($value) == 'integer' ) {
                $types .= 'i';
            } else {
                $types .= 's';
            }
            if($count++ > 0){
                $query .= "\n ,";
            }
            $query .= " " . $key . " = ? ";
        }
        $this->query($query, $types, $params);
    }
    
}